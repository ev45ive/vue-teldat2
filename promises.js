function echo(msg,callback){
    setTimeout(()=>{
        callback(msg)
    },1000)
}

echo('lubie', res => {
    echo(res + ' placki', res => {
        console.log(res)
    })
})

function pecho(msg){
    return new Promise((resolve)=>{
       setTimeout(()=>{
         resolve(msg)
       },1000)
    })
}
p = pecho('lubie')
.then(res => res + ' placki')
.then(console.log);

// p.then( resp => {
//     pecho(resp + ' promisy').then(console.log)
// })

pecho('lubie')
.then( resp => pecho(resp + ' promisy'))
.then(console.log)

// Promise {<pending>}
// lubie placki
// VM32266:9 lubie placki
// lubie promisy


fetch('http://localhost:8080/test.json')
.then((p)=>p.json())
.then(console.log)