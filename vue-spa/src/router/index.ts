import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Playlists from '../views/Playlists.vue'
import MusicSearch from '../views/MusicSearch.vue'
import AlbumDetails from '../views/AlbumDetails.vue'

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/playlists/:playlist_id?',
    name: "Playlists",
    component: Playlists,
    children: [
      {
        // path: /* '/playlists' + '/' + */ '/playlists/home',
        path: /* '/playlists' + '/' + */ 'home',
        component: Home
      }
    ]
  },
  {
    path: '/search',
    name: "Search",
    component: MusicSearch
  },
  {
    path: '/search/album/:album_id',
    name: "AlbumDetails",
    component: AlbumDetails
  },
  {
    path: "/",
    name: "Home",
    // component: Home
    // redirect: '/playlists',
    redirect: { name: 'Playlists' }
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: '**',
    component: {
      render: h => h('div', {}, '404 Not Found')
    }
  }
];

const router = new VueRouter({
  // mode: "hash",
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active'
});

export default router;
