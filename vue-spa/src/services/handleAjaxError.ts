import { auth } from './';
import { throwError, Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import { catchError } from 'rxjs/operators';

export const handleAjaxError = () => <T>(obs: Observable<T>) => obs.pipe(
  catchError((err: any) => {
    if (err instanceof AjaxError) {
      if (err.status == 401) {
        auth.authorize();
      }
      return throwError(new Error(err.response.error.message));
    }
    return throwError(err);
    // return of(this.results as Album[])
    // return EMPTY
  })
);
