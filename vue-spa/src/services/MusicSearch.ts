import { auth } from './';
import { AlbumSearchResult, Album } from '@/model/Search';
import { from, EMPTY, of, Subject, merge, scheduled, ReplaySubject, BehaviorSubject, Observable, pipe } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import { map, catchError, mergeAll, startWith, mergeMap, concatMap, exhaustMap, switchMap, concatAll, switchAll, exhaust, withLatestFrom, filter, tap, combineLatest } from 'rxjs/operators'
import { handleAjaxError } from './handleAjaxError';


const retryWhenLoggedIn = <T>() => pipe(combineLatest<T, boolean>(auth.isLoggedIn),
  // withLatestFrom(auth.isLoggedIn),
  // tap(s => {
  //   debugger
  // }),
  filter(([query, loggedIn]) => loggedIn !== false),
  map(([query, loggedIn]) => query))


export class MusicSearch {
  results: Pick<Album, "id" | "name" | "images">[] = [
    {
      id: "123",
      name: "Test Album",
      images: [
        {
          url: "https://www.placecage.com/c/400/400"
        } as any
      ]
    },
    {
      id: "234",
      name: "Test Album 234",
      images: [
        {
          url: "https://www.placecage.com/c/400/400"
        } as any
      ]
    },
    {
      id: "567",
      name: "Test Album 345",
      images: [
        {
          url: "https://www.placecage.com/c/400/400"
        } as any
      ]
    },
    {
      id: "345",
      name: "Test Album 456 <b>placki</b>",
      images: [
        {
          url: "https://www.placecage.com/c/400/400"
        } as any
      ]
    }
  ];

  queryChange = new BehaviorSubject<string>('batman')
  resultsChange = new BehaviorSubject<Album[]>(this.results as Album[])
  recentErrors = new ReplaySubject(3, 10_000)

  constructor() {
    (window as any).subject = this.resultsChange

    this.queryChange.pipe(

      retryWhenLoggedIn(),

      map(query => new URLSearchParams({
        type: 'album', q: query
      })),

      switchMap(params => { // all concurently
        return ajax.getJSON<AlbumSearchResult>(
          `https://api.spotify.com/v1/search?${params}`, {
          'Authorization': 'Bearer ' + auth.getToken()
        }).pipe(
          map(resp => resp.albums.items),
          handleAjaxError(),
          catchError(err => { this.recentErrors.next(err); return EMPTY })
        )
      })
    ).subscribe(this.resultsChange)
  }

  searchAlbums(query: string) {
    this.queryChange.next(query)
  }


  fetchAlbum(id: Album['id']) {
    return ajax.getJSON<AlbumSearchResult>(
      `https://api.spotify.com/v1/albums/${id}`, {
      'Authorization': 'Bearer ' + auth.getToken()
    }).pipe(
      handleAjaxError()
    )
  }

  searchArtists(query: string) {

    return new Promise(callback => {
      setTimeout(() => {
        // callback(response)
      }, 2000)
    })
  }


  searchPlaylits(query: string) {

    return new Observable((observer) => {
      var i = 0;
      const handler = setInterval(() => {
        // callback(response)
        // observer.next(response)
        // observer.error()
        // if(i++ > 5 )
        // observer.complete()
      }, 2000)
      // Teardown / unsubscribe handler
      return () => { clearInterval(handler) }
    })

    /* searchPlaylits('placki').pipe(a(),b()).subscribe({
      next: () => {},
      error: () => {},
      complete: () => {},
    }) 
    .unsubscribe() // <- cancel 

    myAB = obs => obs.pipe(a(),b())
    myAB = () => pipe(a(),b())

    // ===
    // unicast => multicast
    mySubject.subscribe(...)
    mySubject.subscribe(...)
    mySubject.subscribe(...)
    searchPlaylits('placki').pipe(myAB()).subscribe(mySubject)
    */
  }


  // getResults() {
  //   return this.resultsChange.pipe(startWith(this.results))
  // }

  // searchAlbums(query: string) {
  //   const params = new URLSearchParams({
  //     type: 'album', q: query
  //   })
  //   type ErrorResponse = {
  //     error: any;
  //   };

  //   return fetch(`https://api.spotify.com/v1/search?${params}`, {
  //     headers: {
  //       'Authorization': 'Bearer ' + auth.getToken()
  //     }
  //   })
  //     .then(resp => {
  //       return resp.json().then(data => {
  //         if (resp.ok) {
  //           return data as AlbumSearchResult;
  //         } else {
  //           if (resp.status == 401) {
  //             auth.authorize()
  //           }
  //           return Promise.reject(data.error)
  //         }
  //       })
  //     })
  //     .then(data => data.albums.items)
  // }


}

