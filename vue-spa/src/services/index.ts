import { PlaylistsService } from './PlaylistsService';
import { MusicSearch } from './MusicSearch';
import { Auth } from './Auth';

export const playlistsService = new PlaylistsService()


export const auth = new Auth({
  auth_url: 'https://accounts.spotify.com/authorize',
  client_id: 'b25cd792c2734bce8b0a125d793ab3e7',
  redirect_uri: 'http://localhost:8080/',
  response_type: 'token',
  show_dialog: true,
  scopes: [
    'playlist-read-collaborative',
    'playlist-modify-public',
    'playlist-read-private',
    'playlist-modify-private',
  ]
})


export const musicSearch = new MusicSearch();
