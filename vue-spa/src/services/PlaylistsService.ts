import { Playlist } from '@/model/Playlist';
import { BehaviorSubject, of, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { UserProfile } from './Auth';
import { auth } from '.';
import { PagingObject } from '@/model/PagingObject';

export class PlaylistsService {
  entities = new BehaviorSubject<{ [id: string]: Playlist }>({
    '123': {
      id: "123",
      name: "Vue Hits",
      public: true,
      description: "..."
    },
    '234': {
      id: "234",
      name: "Vue TOP 20",
      public: false,
      description: "...Vue TOP 20"
    },
    '345': {
      id: "345",
      name: "Best of Vue",
      public: true,
      description: "..."
    }
  })

  order = new BehaviorSubject(['123', '234', '345'])
  favouite = new BehaviorSubject(['123', '234', '345'])
  recent = new BehaviorSubject(['123', '234', '345'])

  playlistsChange = combineLatest(
    this.entities, this.order
  ).pipe(map(([entities, order]) => order.map(id => entities[id])))

  savePlaylist(draft: Playlist) {
    const { name, public: isPublic, description } = draft;
    return ajax({
      method: 'PUT',
      url: `https://api.spotify.com/v1/playlists/${draft.id}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + auth.getToken()
      },
      body: {
        name, public: isPublic, description
      }
    }).pipe(map(resp => {
      this.updatePlaylist(draft)
      return draft;
    }))
  }

  updatePlaylist(draft: Playlist) {
    const entities = this.entities.getValue()
    entities[draft.id] = draft;
    this.entities.next(entities)
  }

  fetchUserPlaylists(user_id: UserProfile['id']) {
    return ajax.getJSON<PagingObject<Playlist>>(
      `https://api.spotify.com/v1/users/${user_id}/playlists`, {
      'Authorization': 'Bearer ' + auth.getToken()
    })
  }

  getUserPlaylists(user_id: UserProfile['id']) {
    this.fetchUserPlaylists(user_id)
      .subscribe(playlists => {

        this.entities.next(playlists.items.reduce((entities, p) => {
          entities[p.id] = p;
          return entities
        }, this.entities.getValue()))

        this.order.next(playlists.items.map(p => p.id))
      })
    return this.playlistsChange
  }

  fetchPlaylist(playlist_id: Playlist['id']) {
    return ajax.getJSON<PagingObject<Playlist>>(
      `https://api.spotify.com/v1/playlists/${playlist_id}`, {
      'Authorization': 'Bearer ' + auth.getToken()
    })
  }

  getPlaylist(playlist_id: Playlist['id']) {
    return this.entities.pipe(map(e => e[playlist_id]))
  }

  // getPlaylists() {
  //   return this.playlists.asObservable()
  // }

}

