import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import UserProfile from './components/UserProfile.vue'
import Card from './components/Card.vue'
import VueRx from 'vue-rx'
Vue.use(VueRx)

Vue.config.productionTip = false;


Vue.filter('yesno', (value: boolean, yes = "Yes", no = "No") => {
  return value ? yes : no;
})

Vue.component('SuperButton', {
  render: h => h('button', ['Click me'])
})

Vue.component('UserProfile', UserProfile)
Vue.component('Card', Card)

new Vue({
  router,
  store,
  render: h => h(App)
})
  .$mount("#app");
