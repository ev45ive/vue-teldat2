import { Track } from './Track';

export interface Entity {
  id: string;
  name: string;
}
export interface Playlist extends Entity {
  // type: 'playlist'
  public: boolean;
  description: string;
  // tracks: Array<Track>
  /**
   * List of tracks
   * @see {Track}
   */
  tracks?: Track[]
}

// const pt: Playlist | Track = {}

// if (pt.type === 'playlist') {
//   pt.tracks
// } else {
//   pt.duration
// }

// const p: Playlist = {
//   id: '123',
//   description: '', name: '', public: true,
//   tracks: []
// }


// if (p.tracks) {
//   p.tracks.length
// }

// const len = p.tracks && p.tracks.length
// const len2 = p.tracks?.length


// class Dot implements Point{
//   x = 1
//   y = 1
// }

// interface Point { x: number, y: number }
// interface Vector { x: number, y: number, length:number }

// let p: Point = { x: 123, y: 123 };
// let v: Vector = { x: 123, y: 123, length:123 };

// p = v;
// v = p;