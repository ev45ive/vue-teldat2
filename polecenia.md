# Clone and run
cd ..
git clone https://bitbucket.org/ev45ive/vue-teldat2.git 
cd vue-teldat2/vue-spa
npm i
npm run serve

# NPM
npm init
npm i vue http-server

# Instalacje
https://nodejs.org/en/
node -v
v14.5.0

npm -v
6.14.5

https://code.visualstudio.com/
code -v 
1.40.1
8795a9889db74563ddd43eb0a897a2384129a619
x64

https://git-scm.com/download/win
git --version
git version 2.23.0.windows.1

# Nowy projekt
npm i -g @vue/cli

vue create vue-spa

Vue CLI v4.5.3
? Please pick a preset: Manually select features
? Check the features needed for your project: Choose Vue version, Babel, TS, PWA, Router, Vuex, CSS Pre-processors, Linter, Unit, E2E
? Choose a version of Vue.js that you want to start the project with 2.x
? Use class-style component syntax? Yes
? Use Babel alongside TypeScript (required for modern mode, auto-detected polyfills, transpiling JSX)? Yes
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features:
? Pick a unit testing solution: Jest
? Pick an E2E testing solution: Cypress
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? Yes
? Save preset as: teldat-ts-all

cd vue-spa
npm run serve


# Bootstrap
cd vue-spa
npm i bootstrap
import 'bootstrap/dist/css/bootstrap.css'


# Vue TS decorators
https://github.com/kaorun343/vue-property-decorator#Provide

# Lifecycle
https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram

# REST API + OAuth2
https://developer.spotify.com/documentation/web-api/reference/users-profile/get-current-users-profile/

# PWA
https://cli.vuejs.org/core-plugins/pwa.html#configuration
https://developers.google.com/web/tools/workbox/guides/generate-service-worker/webpack

# Slots
https://vuejs.org/v2/guide/components-slots.html

# RxJS
https://rxjs-dev.firebaseapp.com/operator-decision-tree
https://rxjs-dev.firebaseapp.com/guide/operators
https://rxjs-dev.firebaseapp.com/api/webSocket/webSocket
https://lawsofux.com/doherty-threshold
https://www.google.com/search?q=everything+is+a+stream


# Rx marbles
https://rxmarbles.com/#withLatestFrom
https://rxviz.com/

# Vue Rx
https://github.com/vuejs/vue-rx


# Vue Router
https://router.vuejs.org/
